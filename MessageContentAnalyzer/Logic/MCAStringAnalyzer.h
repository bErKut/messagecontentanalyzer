//
//  MCAStringAnalyzer.h
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 27/04/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MCAStringAnalyzerDelegate, MCAContentSearcherProvider;

@interface MCAStringAnalyzer : NSObject

@property (nonatomic, weak) id <MCAStringAnalyzerDelegate> delegate;

- (instancetype)initWithProviders:(NSArray <id <MCAContentSearcherProvider>> *)searchers;
- (void)consumeString:(NSString *)string
           completion:(void (^)( NSString * _Nullable JSON, NSError  * _Nullable  error))completion
        callbackQueue:(nullable NSOperationQueue *)queue;

@end

@interface MCAStringAnalyzer(unavailable)

- (instancetype)init UNAVAILABLE_ATTRIBUTE;
+ (instancetype)new;

@end

@protocol MCAStringAnalyzerDelegate <NSObject>

- (void)stringAnalyzer:(MCAStringAnalyzer *)analyzer didFinishWithResult:(nullable NSString *)result;

@end

NS_ASSUME_NONNULL_END