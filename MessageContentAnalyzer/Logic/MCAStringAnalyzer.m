//
//  MCAStringAnalyzer.m
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 27/04/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import "MCAStringAnalyzer.h"
#import "MCAContentSearcherProvider.h"
#import "MCAContentSearcherOperation.h"

@interface MCAStringAnalyzer()

@property (nonatomic, strong) NSString *currentString;
@property (nonatomic, strong) NSMutableDictionary *contents;
@property (nonatomic, strong) NSArray *providers;
@property (nonatomic, strong) NSOperationQueue *workingQueue;
@property (nonatomic, strong) NSLock *lock;

@end

@implementation MCAStringAnalyzer

- (instancetype)initWithProviders:(NSArray <id <MCAContentSearcherProvider>> *)searchers
{
    NSParameterAssert(searchers);
    NSAssert(searchers.count, @"shouldn't be empty");
    
    if (self = [super init])
    {
        _providers = searchers;
        _contents = [NSMutableDictionary new];
        _lock = [NSLock new];
    }
    
    return self;
}

- (void)consumeString:(NSString *)string
           completion:(void (^)(NSString * _Nullable, NSError * _Nullable))completion
        callbackQueue:(NSOperationQueue *)queue
{
    NSParameterAssert(string);
    NSParameterAssert(completion);
    
    NSBlockOperation *completionOperation = [NSBlockOperation blockOperationWithBlock:^{
        NSLog(@"analyzer completion!");
        NSOperationQueue *completionQueue = queue;
        if (!completionQueue) completionQueue = [NSOperationQueue mainQueue];
        [completionQueue addOperationWithBlock:^{
            NSError *error = nil;
            NSString *jsonString = nil;
            NSDictionary *contents = [_contents copy];
            if (contents.count)
            {
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:contents
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:&error];
                if (jsonData)
                {
                    jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                }
                else if (error)
                {
                    NSLog(@"json serialization failure: %@", [error localizedDescription]);
                }
                
            }
            else
            {
                jsonString = @"";
            }

            completion(jsonString, error);
        }];
    }];
    
    [self.workingQueue cancelAllOperations];
    self.workingQueue.suspended = YES;
    for (id <MCAContentSearcherProvider> searchProvider in self.providers)
    {
        NSOperation <MCAContentSearcherOperation> *searchOperation = [searchProvider operationFromString:string];
        __weak __typeof (searchOperation) weakOperation = searchOperation;
        searchOperation.completionBlock = ^(){
            [self.lock lock];
            if ([weakOperation contents])
            {
                [self.contents setObject:[weakOperation contents]
                                  forKey:[weakOperation contentLabel]];
            }
            else
            {
                [self.contents removeObjectForKey:[weakOperation contentLabel]];
            }
            [self.lock unlock];
        };
        
        [self.workingQueue addOperation:searchOperation];
        [completionOperation addDependency:searchOperation];
    }
    
    [self.workingQueue addOperation:completionOperation];
    self.workingQueue.suspended = NO;
}

- (NSOperationQueue *)workingQueue
{
    if (!_workingQueue)
    {
        _workingQueue = [NSOperationQueue new];
        _workingQueue.name = [NSString stringWithFormat:@"com.MessageContentAnalyzer.MCAStringAnalyzer%p.workingQueue", self];
        _workingQueue.maxConcurrentOperationCount = 4;
    }
    
    return _workingQueue;
}


@end
