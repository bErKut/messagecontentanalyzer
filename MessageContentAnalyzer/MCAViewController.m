//
//  ViewController.m
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 27/04/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import "MCAViewController.h"
#import "MCAStringAnalyzer.h"
#import "MCAProvidersFactory.h"

@interface MCAViewController () <UITextViewDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) MCAStringAnalyzer *analyzer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UITextView *inputView;
@property (weak, nonatomic) IBOutlet UITextView *resultView;
@property (assign, nonatomic) CGFloat prevContentHeight;
@property (assign, nonatomic) CGFloat currentKeyboardHeight;
@property (weak, nonatomic) IBOutlet UIView *pageControlContainer;

@end

@implementation MCAViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureAnalyzer];
    [self registerForKeyboardNotifications];
    [self mountTapHandling];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.prevContentHeight = self.inputView.contentSize.height;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showNextPageAnimation];
    });
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Keyboard notifications

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.currentKeyboardHeight = kbSize.height;
}

- (void)keyboardWillHide:(NSNotification*)notification {
    self.currentKeyboardHeight = 0.0f;
}

#pragma mark - Tap Gesture

- (void)mountTapHandling
{
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
    [self.view addGestureRecognizer:tapRecognizer];
}

- (void)onTap:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
}

#pragma mark - String analyzing

- (void)configureAnalyzer
{
    id <MCAContentSearcherProvider> mentions = [MCAProvidersFactory contentProviderForType:MCAProviderTypeMentions];
    id <MCAContentSearcherProvider> emoticons = [MCAProvidersFactory contentProviderForType:MCAProviderTypeEmoticons];
    id <MCAContentSearcherProvider> links = [MCAProvidersFactory contentProviderForType:MCAProviderTypeLinks];
    self.analyzer = [[MCAStringAnalyzer alloc] initWithProviders:@[mentions, emoticons, links]];
}

- (void)analyzeString:(NSString *)string
{
    [self.analyzer consumeString:string
                      completion:^(NSString * _Nullable JSON, NSError * _Nullable error) {
                          self.resultView.text = JSON;
                      }
                   callbackQueue:[NSOperationQueue mainQueue]];
    
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [self analyzeString:textView.text];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    // delay is needed for retrieving modified text form textView.text(otherwise it will be old)
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        CGFloat contentHeight = textView.contentSize.height;
        CGPoint point = self.inputView.frame.origin;
        CGPoint inputOrigin = [self.view convertPoint:point fromView:self.scrollView];
        CGFloat availableHeight = CGRectGetHeight(self.view.frame) - (self.currentKeyboardHeight + inputOrigin.y);
        self.inputViewHeightConstraint.constant = MIN(availableHeight, contentHeight);
        
        if (text.length)
        {
            unichar changedCharacter = [text characterAtIndex:0];
            if ([[NSCharacterSet whitespaceCharacterSet] characterIsMember:changedCharacter])
            {
                [self analyzeString:textView.text];
            }
        }
        else
        {
            [self analyzeString:textView.text];
        }

    });
    
    return YES;
}

#pragma mark - animation

- (void)showNextPageAnimation
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"bounds.origin.x"];
    animation.duration = 1.0;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    animation.values = @[@0, @(self.view.frame.size.width/4), @(0)];
    animation.keyTimes = @[@0, @0.5, @1];
    animation.additive = YES;
    
    [self.scrollView.layer addAnimation:animation forKey:@"bounds.origin.x"];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    self.pageControl.currentPage = roundf(self.scrollView.contentOffset.x/CGRectGetWidth(self.scrollView.bounds));
    if (self.scrollView.contentOffset.x >= CGRectGetWidth(self.scrollView.bounds))
        [self.inputView resignFirstResponder];
}

@end
