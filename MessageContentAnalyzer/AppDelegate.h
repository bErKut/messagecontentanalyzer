//
//  AppDelegate.h
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 27/04/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

