//
//  MCAProvidersFactory.m
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 11/05/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import "MCAProvidersFactory.h"
#import "MCAContentSearcherProvider.h"
#import "MCAMentionsSearcherProvider.h"
#import "MCAEmoticonsSearcherProvider.h"
#import "MCALinksSearcherProvider.h"


@implementation MCAProvidersFactory

+ (id <MCAContentSearcherProvider>)contentProviderForType:(MCAProviderType)type
{
    id <MCAContentSearcherProvider> provider = nil;
    switch (type)
    {
        case MCAProviderTypeMentions:
            provider = [MCAMentionsSearcherProvider new];
            break;
            
        case MCAProviderTypeEmoticons:
            provider = [MCAEmoticonsSearcherProvider new];
            break;
            
        case MCAProviderTypeLinks:
            provider = [MCALinksSearcherProvider new];
            break;
            
        default:
            break;
    }
    
    return provider;
}

@end
