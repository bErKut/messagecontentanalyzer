//
//  MCAEmoticonsSearcher.m
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 27/04/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import "MCAEmoticonsSearcherProvider.h"
#import "MCAEmoticonsOperation.h"

@implementation MCAEmoticonsSearcherProvider

#pragma mark - MCAContentSearcherProvider

- (NSOperation<MCAContentSearcherOperation> *)operationFromString:(NSString *)string
{
    NSParameterAssert(string);
    MCAEmoticonsOperation *operation = [MCAEmoticonsOperation new];
    [operation consume:string];
    return operation;
}

@end
