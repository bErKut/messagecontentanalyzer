//
//  MCAContentSearcher.h
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 27/04/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MCAContentSearcherOperation;


@protocol MCAContentSearcherProvider <NSObject>

- (NSOperation <MCAContentSearcherOperation> *)operationFromString:(NSString *)string;

@end

NS_ASSUME_NONNULL_END
