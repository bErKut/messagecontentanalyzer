//
//  MCAProvidersFactory.h
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 11/05/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MCAProviderType) {
    MCAProviderTypeMentions,
    MCAProviderTypeEmoticons,
    MCAProviderTypeLinks
};

@protocol MCAContentSearcherProvider;

@interface MCAProvidersFactory : NSObject

+ (id <MCAContentSearcherProvider>)contentProviderForType:(MCAProviderType)type;

@end
