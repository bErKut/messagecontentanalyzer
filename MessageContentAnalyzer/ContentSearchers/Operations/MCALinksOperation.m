//
//  MCALinksOperation.m
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 03/05/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import "MCALinksOperation.h"

@implementation MCALinksOperation
{
    BOOL _executing;
    BOOL _finished;
}

#pragma mark - MCAContentSearcherOperation

- (NSString *)contentLabel
{
    return @"links";
}

#pragma mark - override

- (instancetype)init
{
    if (self = [super init])
    {
        _executing = NO;
        _finished = NO;
    }
    
    return self;
}

- (void)start
{
    if ([self isCancelled])
    {
        [self willChangeValueForKey:@"isFinished"];
        _finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }
    
    [self willChangeValueForKey:@"isExecuting"];
    _executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
    [self main];
}

- (void)main
{
    NSURLSession *session = [NSURLSession sharedSession];
    __weak typeof (self) weakSelf = self;
    NSMutableArray *urls = [[self urlsInString:_string] mutableCopy];
    if (!urls.count)
    {
        [self complete];
        return;
    }
    
    //    NSArray *urls = @[@"http://thesoftmoon.bandcamp.com/album/deeper-2"];
    NSMutableSet *tasksURLs = [NSMutableSet new];
    NSMutableArray *contents = [NSMutableArray new];
    [urls enumerateObjectsUsingBlock:^(NSString * _Nonnull urlString, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSURLSessionDataTask *task = [session dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            // cancel tasks if analyzing was stopped
            __strong typeof (weakSelf) strongSelf = weakSelf;
            if (!strongSelf || strongSelf.isCancelled)
            {
                for (NSURLSessionDataTask *task in tasksURLs)
                    [task cancel];
                
                [strongSelf complete];
                
                return;
            }
            
            if (!error)
            {
                NSString *docTitle = @"";
                if (data)
                {
                    NSString *content = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    NSString *startPoint = @"<title>";
                    NSString *endPoint = @"</title>";
                    NSRange startRange = [content rangeOfString:startPoint];
                    NSRange endRange = [content rangeOfString:endPoint];
                    
                    NSString *title = [content substringWithRange:NSMakeRange(startRange.location + startRange.length, endRange.location - (startRange.location + startRange.length))];

                    if (title) docTitle = title;
                }
                
                if (docTitle)
                    [contents addObject:@{@"url": urlString, @"title": docTitle}];
                else
                    [contents addObject:@{@"url": urlString}];
            }
            else
            {
                NSLog(@"title request failed with error: %@", [error localizedDescription]);
            }
            
            [urls removeObject:urlString];
            if (urls.count == 0)
            {
                strongSelf->_contents = [contents copy];
                [strongSelf complete];
            }
        }];
        
        [task resume];
        [tasksURLs addObject:task];
    }];
}

- (BOOL)isConcurrent
{
    return NO;
}

- (BOOL)isExecuting
{
    return _executing;
}

- (BOOL)isFinished
{
    return _finished;
}

- (BOOL)isAsynchronous
{
    return YES;
}

#pragma mark - Custom

- (nullable NSArray <NSString *> *)urlsInString:(nonnull NSString *)string
{
    NSMutableArray *urls = [NSMutableArray new];
    NSArray *words = [string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    for (NSString *word in words)
    {
        if ([self isStringValidURL:word])
            [urls addObject:word];
    }
    return [urls copy];
}

- (BOOL)isStringValidURL:(nonnull NSString *)candidate
{
    NSURL *url = [NSURL URLWithString:candidate];
    if (url && url.scheme.length && url.host.length)
    {
        if ([url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"])
            return YES;
    }

    return NO;
}

- (void)complete
{
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    
    _executing = NO;
    _finished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

@end
