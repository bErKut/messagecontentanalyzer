//
//  MCAContentOperationBase.h
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 03/05/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MCAContentSearcherOperation.h"

@interface MCAContentOperationBase : NSOperation <MCAContentSearcherOperation>
{
    NSArray *_contents;
    NSString *_string;
}

@end
