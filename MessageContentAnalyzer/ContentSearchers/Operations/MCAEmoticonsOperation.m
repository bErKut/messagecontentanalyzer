//
//  MCAEmoticonsOperation.m
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 03/05/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import "MCAEmoticonsOperation.h"

static const NSUInteger kEmoticonMaxLength = 15;

@implementation MCAEmoticonsOperation

#pragma mark - MCAContentSearcherOperation

- (NSString *)contentLabel
{
    return @"emoticons";
}

#pragma mark - Overriden methods

- (void)main
{
    NSMutableArray *emoticons = [NSMutableArray new];
    NSMutableString *emoticonString = nil;
    for (NSUInteger i = 0; i < _string.length; i++)
    {
        unichar c = [_string characterAtIndex:i];
        if (c == '(')
        {
            emoticonString = [NSMutableString new];
        }
        else if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:c])
        {
            emoticonString = nil;
        }
        else if ([[NSCharacterSet alphanumericCharacterSet] characterIsMember:c])
        {
            if (emoticonString && emoticonString.length < kEmoticonMaxLength)
            {
                [emoticonString appendFormat:@"%c", c];
            }
            else
            {
                emoticonString = nil;
            }
        }
        else if (c == ')')
        {
            if (emoticonString)
            {
                [emoticons addObject:[emoticonString copy]];
                emoticonString = nil;
            }
        }
    }
    
    if (emoticons.count)
        _contents = [emoticons copy];
}

@end
