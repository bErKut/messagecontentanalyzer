//
//  MCAContentOperationBase.m
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 03/05/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import "MCAContentOperationBase.h"

@implementation MCAContentOperationBase

#pragma mark - MCAContentSearcherOperation

- (void)consume:(NSString *)string
{
    NSParameterAssert(string);
    _string = [string copy];
}

- (NSArray *)contents
{
    return _contents;
}

- (NSString *)contentLabel
{
    return @"";
}

@end
