//
//  MCAContentSearcherOperation.h
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 03/05/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MCAContentSearcherOperation <NSObject>

- (void)consume:(nonnull NSString *)string;
- (nullable NSArray *)contents;
- (nonnull NSString *)contentLabel;

@end
