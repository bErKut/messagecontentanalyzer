//
//  MCAMentionsOperation.m
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 03/05/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import "MCAMentionsOperation.h"

@implementation MCAMentionsOperation

#pragma mark - MCAContentSearcherOperation

- (NSString *)contentLabel
{
    return @"mentions";
}

#pragma mark - Overriden methods

- (void)main
{
    NSMutableArray *mentions = [NSMutableArray new];
    NSMutableCharacterSet *separationSet = [NSMutableCharacterSet whitespaceAndNewlineCharacterSet];
    [separationSet formUnionWithCharacterSet:[NSCharacterSet characterSetWithCharactersInString:@".,:'"]];
    NSArray *components = [_string componentsSeparatedByCharactersInSet:separationSet];
    for (NSString *component in components)
    {
        if (component.length > 1 && [component characterAtIndex:0] == '@')
        {
            NSCharacterSet *atSet = [NSCharacterSet characterSetWithCharactersInString:@"@"];
            [mentions addObject:[component stringByTrimmingCharactersInSet:atSet]];
        }
    }
    
    if (mentions.count)
        _contents = [mentions copy];
}

@end
