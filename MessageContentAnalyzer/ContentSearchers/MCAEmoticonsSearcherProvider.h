//
//  MCAEmoticonsSearcher.h
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 27/04/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MCAContentSearcherProvider.h"

@interface MCAEmoticonsSearcherProvider : NSObject <MCAContentSearcherProvider>

@end
