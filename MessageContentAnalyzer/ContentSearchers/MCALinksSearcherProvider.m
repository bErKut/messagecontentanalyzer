//
//  MCALinksSearcher.m
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 27/04/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import "MCALinksSearcherProvider.h"
#import "MCALinksOperation.h"

@implementation MCALinksSearcherProvider

#pragma mark - MCAContentSearcherProvider

- (NSOperation<MCAContentSearcherOperation> *)operationFromString:(NSString *)string
{
    NSParameterAssert(string);
    
    MCALinksOperation *operation = [MCALinksOperation new];
    [operation consume:string];
    return operation;
}

@end
