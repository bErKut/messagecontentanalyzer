//
//  MCAMentionsSearcher.m
//  MessageContentAnalyzer
//
//  Created by Vlad Berkuta on 27/04/16.
//  Copyright © 2016 Vlad Berkuta. All rights reserved.
//

#import "MCAMentionsSearcherProvider.h"
#import "MCAMentionsOperation.h"


@implementation MCAMentionsSearcherProvider

#pragma mark - MCAContentSearcherProvider

- (NSOperation<MCAContentSearcherOperation> *)operationFromString:(NSString *)string
{
    NSParameterAssert(string);
    
    MCAMentionsOperation *operation = [[MCAMentionsOperation alloc] init];
    [operation consume:string];
    return operation;
}

@end
